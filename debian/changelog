bibtex2html (1.99-4) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * Remove Samuel from Uploaders
  * Add Rules-Requires-Root: no
  * Bump Standards-Version to 4.6.2
  * Depend on ocaml instead of transitional ocaml-nox

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.

 -- Stéphane Glondu <glondu@debian.org>  Sat, 02 Sep 2023 08:47:21 +0200

bibtex2html (1.99-3) unstable; urgency=medium

  * Make this an architecture-dependent package:
    - set Architecture=any
    - fix patch charset, to use the unix module also for compilation to
      native code.
    - debian/rules: auto_build override conditional on whether this is an
      ocaml native architecture or not
    - debian/rules: auto_test override conditional on whether this is an
      ocaml native architecture or not
    - Depends: add {shlibs:Depends}
  * debian/ruies : drop override of dh_auto_install: we don't need it anymore
    since the upstream Makefile does the right thing for native and bytecode
    architectures.
  * debian/rules: drop override of dh_strip which is no longer needed.
  * Apply patch by James Van Zandt that adds many translations of
    math-related symbols. Thanks a lot for the patch! (closes: #921278)
  * Apply patch by James Van Zandt that adds translations of
    accentuated characters. Thanks a lot for the patch! (closes: #921593)
  * Standards-version 4.4.1 (no change)
  * Build-depend on debhelper-compat instead of debhelper. Remove
    debian/compat file.
  * Debhelper compatibility level 12.
  * debian/copyright: update copyright years.

 -- Ralf Treinen <treinen@debian.org>  Sun, 13 Oct 2019 12:14:52 +0200

bibtex2html (1.99-2) unstable; urgency=medium

  * Update Vcs-* to salsa
  * Standards-Version 4.2.1 (no change)

 -- Ralf Treinen <treinen@debian.org>  Tue, 04 Dec 2018 09:09:09 +0100

bibtex2html (1.99-1) unstable; urgency=medium

  * New upstream version.
    - refreshed patches charset and test-byte
  * Advance build-dependency on ocaml to >= 4.03, as indicated by upstream.
  * Standards-version 4.1.3
    -d/copyright: https in format header.
  * Debhelper compatibility level 11.
  * New patch manual-no-external-url: remove external url from manual.tex

 -- Ralf Treinen <treinen@debian.org>  Fri, 23 Mar 2018 21:29:59 +0100

bibtex2html (1.98-6) unstable; urgency=medium

  * Standards-Version 4.1.0 (no change)
  * Drop obsolete version constraint on build-dependency dh-ocaml

 -- Ralf Treinen <treinen@debian.org>  Thu, 21 Sep 2017 05:39:45 +0200

bibtex2html (1.98-5) unstable; urgency=medium

  * debhelper compatibility level 10:
    - bump build-dependency on debhelper
    - bump value in debian/compat

 -- Ralf Treinen <treinen@debian.org>  Thu, 27 Oct 2016 09:09:53 +0200

bibtex2html (1.98-4) unstable; urgency=medium

  * test bib2bib: compare files with "-a" to force comparison as text
  * d/control: drop Testsuite field which is no longer needed
  * Standards-version 3.9.8 (no change)
  * Vcs-git: use https
  * fixed two typos in manpages

 -- Ralf Treinen <treinen@debian.org>  Fri, 15 Apr 2016 19:18:46 +0200

bibtex2html (1.98-3) unstable; urgency=medium

  [ Rémi Vanicat ]
  * Remove myself from Uploaders

  [ Mehdi Dogguy ]
  * Team upload.
  * Recompile with OCaml 4.02.3.

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 10 Oct 2015 18:54:28 +0200

bibtex2html (1.98-2) unstable; urgency=low

  * Fix URLs in Vcs-* fields
  * Standards-Version 3.9.6 (no change)
  * add DEP8-style package tests:
    - bib2bib
    - bibtex2html

 -- Ralf Treinen <treinen@debian.org>  Wed, 10 Jun 2015 15:44:22 +0200

bibtex2html (1.98-1) unstable; urgency=medium

  * New upstream version.
  * Refreshed patches: charset, path-expand-styles
  * Update patch charset: we also have to link with unix.cma
  * Drop patch paranoid-bibtex, this has been fixed upstream.

 -- Ralf Treinen <treinen@debian.org>  Sun, 06 Jul 2014 19:58:44 +0200

bibtex2html (1.97-5) unstable; urgency=low

  * Standards-Version 3.9.5 (no change)
  * Recompile with ocaml 4.01.0 (no source change)

 -- Ralf Treinen <treinen@debian.org>  Tue, 03 Dec 2013 22:20:05 +0100

bibtex2html (1.97-4) unstable; urgency=low

  * Upload to unstable.

 -- Ralf Treinen <treinen@debian.org>  Tue, 07 May 2013 08:44:03 +0200

bibtex2html (1.97-3) experimental; urgency=low

  [Sylvain Le Gall]
  * Remove Sylvain Le Gall from uploaders

  [ Ralf Treinen ]
  * Patch path-expand-styles: expand the full path name of bibtex styles
    that are defined by files in the working directory (closes: #698004).
  * Standards-Version 3.9.4 (no change).

 -- Ralf Treinen <treinen@debian.org>  Sun, 13 Jan 2013 21:21:24 +0100

bibtex2html (1.97-2) unstable; urgency=medium

  * patch paranoid-bibtex: run bibtex in the directory where the .aux file
    resides (closes: #666572). Upload with urgency medium since older
    bibtex2html will fail with recent TeX.
  * Standards-Version: 3.9.3
    - debian/copyright: machine-readable format 1.0

 -- Ralf Treinen <treinen@debian.org>  Sun, 01 Apr 2012 17:34:08 +0200

bibtex2html (1.97-1) unstable; urgency=low

  * New upstream version

 -- Ralf Treinen <treinen@debian.org>  Tue, 14 Feb 2012 09:33:58 +0100

bibtex2html (1.96-5) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Recompile with ocaml 3.12.1 (no source changes)

  [ Stefano Zacchiroli ]
  * Remove myself from Uploaders

 -- Stéphane Glondu <glondu@debian.org>  Wed, 02 Nov 2011 07:06:15 +0100

bibtex2html (1.96-3) unstable; urgency=low

  * Standards-version 3.9.2 (no change)
  * Patch test-byte: Add to the makefile a test target for byte compilation.
    Add in debian/rules an override to use this instead of test target.

 -- Ralf Treinen <treinen@debian.org>  Mon, 18 Apr 2011 18:53:23 +0200

bibtex2html (1.96-2) experimental; urgency=low

  * debian/rules: set explicitly --mandir so that upstream manpages are
    overwritten by ours (closes: #607716).

 -- Ralf Treinen <treinen@debian.org>  Sun, 02 Jan 2011 19:56:28 +0100

bibtex2html (1.96-1) experimental; urgency=low

  * New upstream release
  * Drop patches sanitise-makefile and clean-targets, they have been
    integrated by upstream
  * Refresh patches
  * debian/manpages/* : fix names of upstream authors
  * Strengthen build-dependency on dh-ocaml to (>= 0.9~) an ocaml-nox to
    (>= 3.11.1-3~) in order to have correct dependency calculation
  * We only have build-depends, not build-depends-indep
  * Standards-Version 3.9.1 (no change)
  * debian/rules: enable again testing, as upstream has fixed it.

 -- Ralf Treinen <treinen@debian.org>  Mon, 04 Oct 2010 17:08:27 +0200

bibtex2html (1.95-1) unstable; urgency=low

  * New upstream version
  * Remove patch strings_utf8, this is fixed by upstream now.
  * Shifted linenumbers of patches according to the new upstream version.
  * Migrated debian/rules from cdbs to dh, removed build-dependency on cdbs.
  * New patch clean-targets: remove more generated files on "make clean"
  * New patch sanitise-makefile: specify shell, rename dist-clean to distclean
  * Generate dependencies from ${ocaml:Depends} and ${perl:Depends}

 -- Ralf Treinen <treinen@debian.org>  Sat, 03 Apr 2010 14:43:55 +0200

bibtex2html (1.94-5) unstable; urgency=low

  * Remove asterisks in debian/NEWS.
  * Recompiled for ocaml 3.11.2

 -- Ralf Treinen <treinen@debian.org>  Mon, 08 Feb 2010 21:49:12 +0100

bibtex2html (1.94-4) unstable; urgency=low

  * manpage bibtex2html(1): escape hyphen
  * Standards-Version 3.8.4 (no change)

 -- Ralf Treinen <treinen@debian.org>  Fri, 29 Jan 2010 08:43:17 +0100

bibtex2html (1.94-3) unstable; urgency=low

  [ Stéphane Glondu ]
  * Fix cdbs includes order

  [ Ralf Treinen ]
  * Convert to source format 3.0 (quilt):
    - no build-depends on dpatch
    - add debian/source/format
    - no patching in debian/rules
    - add debian/patches: quilt style
  * Standards-Version 3.8.3 (no change)
  * Changed doc-base section to Typesetting.

 -- Ralf Treinen <treinen@debian.org>  Thu, 03 Dec 2009 22:15:51 +0100

bibtex2html (1.94-2) unstable; urgency=low

  [ Stephane Glondu ]
  * Switch packaging to git

  [ Ralf Treinen ]
  * Standards-Version 3.8.2 (no change)

 -- Ralf Treinen <treinen@debian.org>  Thu, 25 Jun 2009 21:47:04 +0200

bibtex2html (1.94-1) unstable; urgency=low

  * New upstream release.
  * Debhelper compatibility level 7, adapt build-dependency on debhelper.
  * Standards-Version 3.8.1 (no change).
  * manpages: replace some - by \-

 -- Ralf Treinen <treinen@debian.org>  Thu, 07 May 2009 20:04:48 +0200

bibtex2html (1.93-2) unstable; urgency=low

  [ Ralf Treinen ]
  * Convert to cdbs, add build-dependency on cdbs
  * Add build-dependency on dh-ocaml
  * Add binary dependency on ${misc:Depends}
  * debian/copyright: point to GPL-2, instead of GPL.
  * Source package layout:
    - Rename debian/NEWS.Debian to debian/NEWS to make dh_installchangelogs
      happy.
    - move our own manpages from debian into debian/manpages
    - Rename debian/doc-base.bibtex2html to bibtex2html.doc-base

  [ Stephane Glondu ]
  * Remove Julien from Uploaders

 -- Ralf Treinen <treinen@debian.org>  Sun, 22 Feb 2009 23:11:42 +0100

bibtex2html (1.93-1) unstable; urgency=low

  * New upstream version. This release allows to to sort entries by date
    (closes: Bug#504796).
  * Install aux2bib manpage from debian/rules since upstream seems to
    have dropped it.
  * bib2bibmanpage: document new option "-s $date".

 -- Ralf Treinen <treinen@debian.org>  Sun, 14 Dec 2008 17:49:38 +0100

bibtex2html (1.92-1) unstable; urgency=low

  * New upstream version. This release fixes a bug with accent parsing
    and conversion (closes: Bug#467082).
  * Adapted patch 03_charset to new upstream version.
  * Standards-Version 3.8.0  (no change).

 -- Ralf Treinen <treinen@debian.org>  Tue, 12 Aug 2008 01:43:30 +0200

bibtex2html (1.91-3) unstable; urgency=low

  * Recompile with ocaml 3.10.2

 -- Ralf Treinen <treinen@debian.org>  Sun, 18 May 2008 22:04:07 +0200

bibtex2html (1.91-2) unstable; urgency=low

  [ Ralf Treinen ]
  * Convert doc-base.bibtex2html to UTF8.
  * patch 03_charset: change format to use run directly dpatch-run
  * new patch 01_strings_utf8: print copyright information in utf8.

  [ Stephane Glondu ]
  * Fix bashism in debian/rules (Closes: Bug#477248)

 -- Ralf Treinen <treinen@debian.org>  Wed, 30 Apr 2008 21:30:13 +0200

bibtex2html (1.91-1) unstable; urgency=low

  * New upstream release. This release fixes a bug with protected double-quotes
    in bibtex fields (closes: Bug#466524).
  * debian/copyright: add proper copyright notice.

 -- Ralf Treinen <treinen@debian.org>  Thu, 21 Feb 2008 23:13:29 +0100

bibtex2html (1.90-2) unstable; urgency=low

  * Recompiled for ocaml 3.10.1.
  * Updated Uploaders.

 -- Ralf Treinen <treinen@debian.org>  Thu, 07 Feb 2008 08:48:35 +0100

bibtex2html (1.90-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Fri, 01 Feb 2008 20:04:38 +0100

bibtex2html (1.89-1) unstable; urgency=low

  [ Ralf Treinen ]
  * New upstream release (closes: Bug#459853).
  * Standards-version 3.7.3 (no change).
  * bibtex2html manpage: document new options --revkeys, --unicode,
    --html-entities, --header

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

 -- Ralf Treinen <treinen@debian.org>  Wed, 09 Jan 2008 09:31:14 +0100

bibtex2html (1.88-1) unstable; urgency=low

  * debian/control: use Homepage field for upstream homepage.
  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Sat, 01 Dec 2007 15:31:02 +0100

bibtex2html (1.87-2) unstable; urgency=low

  * Upload to unstable.

 -- Ralf Treinen <treinen@debian.org>  Thu, 30 Aug 2007 20:58:54 +0200

bibtex2html (1.87-1) experimental; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Sat, 07 Jul 2007 10:41:36 +0200

bibtex2html (1.86-3) experimental; urgency=low

  * Rebuild for ocaml 3.10
  * target "clean" in debian/rules: test for existence of Makefile

 -- Ralf Treinen <treinen@debian.org>  Sat, 07 Jul 2007 10:37:53 +0200

bibtex2html (1.86-2) unstable; urgency=low

  * Remove tetex from dependencies.

 -- Ralf Treinen <treinen@debian.org>  Sat, 14 Apr 2007 18:04:51 +0200

bibtex2html (1.86-1) experimental; urgency=low

  * New upstream release

 -- Ralf Treinen <treinen@debian.org>  Wed, 21 Mar 2007 19:12:06 +0100

bibtex2html (1.85-1) unstable; urgency=low

  * New upstream release. Fixes a bug with the --title command line option.
    (closes: Bug#396195).

 -- Ralf Treinen <treinen@debian.org>  Thu,  2 Nov 2006 21:03:34 +0100

bibtex2html (1.84-1) unstable; urgency=low

  * New upstream release.
  * Adapt patch 03_charset to the new Makefile.in

 -- Ralf Treinen <treinen@debian.org>  Tue, 24 Oct 2006 21:42:35 +0200

bibtex2html (1.83-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Wed, 13 Sep 2006 18:27:49 +0200

bibtex2html (1.82-2) unstable; urgency=low

  * Allow cooperation with texlive: depend on "tetex-extra ¦ texlive-base"
    (closes: Bug#381467).
  * Do not generate the documentation in .ps since .html is sufficient.
    As a consequence drop *tex* from build-dependencies.

 -- Ralf Treinen <treinen@debian.org>  Sun, 27 Aug 2006 15:09:18 +0200

bibtex2html (1.82-1) unstable; urgency=low

  * New upstream release.
  * Removed mechanism to build debian/control from debian/control.in
    since we do not need it anymore. Use dh_gencontrol to fill in the
    version of the ocaml runtime system.
  * Removed patch 07_manual_formatting since incorporated now by upstream.

 -- Ralf Treinen <treinen@debian.org>  Wed, 19 Jul 2006 23:13:26 +0200

bibtex2html (1.81-2) unstable; urgency=low

  * (Build-)depends on ocaml 3.09.2

 -- Ralf Treinen <treinen@debian.org>  Tue, 16 May 2006 19:56:22 +0200

bibtex2html (1.81-1) unstable; urgency=low

  * New upstream release.
  * Policy-Version 3.7.2 (no change)
  * Turn build-depends-indep on debhelper, dpatch into build-depends
  * Remove patches that are no longer applied: 01_manpage, 02_ampersand,
    04_manpage_section
  * Patch 07_manual_formatting:
    - call hevea with -fix option to get a table of contents
    - place \title, \author into preamble in order to make hevea happy

 -- Ralf Treinen <treinen@debian.org>  Sun, 14 May 2006 17:34:20 +0200

bibtex2html (1.80-1) unstable; urgency=low

  * New upstream release.
  * Simplify dependency on tetex packages (tetex-extra is sufficient).
  * Write real man pages (closes: Bug#207255).

 -- Ralf Treinen <treinen@debian.org>  Sun, 16 Apr 2006 20:36:57 +0200

bibtex2html (1.79-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Wed,  1 Mar 2006 13:13:05 +0900

bibtex2html (1.78-3) unstable; urgency=low

  * Bumped (Build-)dependency on ocaml to 3.09.1
  * New uploader Sylvain Le Gall <gildor@debian.org>
  * Automate update of debian/control.

 -- Ralf Treinen <treinen@debian.org>  Sat,  7 Jan 2006 10:48:39 +0100

bibtex2html (1.78-1) unstable; urgency=low

  * New upstream release now accepts again @comment{...} entries in
    bibtex files (closes: Bug#343433).

 -- Ralf Treinen <treinen@debian.org>  Fri, 16 Dec 2005 18:22:41 +0100

bibtex2html (1.77-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Fri, 18 Nov 2005 19:49:00 +0100

bibtex2html (1.76-2) unstable; urgency=low

  * Bumped (build-)dependency on ocaml-* to 3.09.0.

 -- Ralf Treinen <treinen@debian.org>  Sat,  5 Nov 2005 14:23:58 +0100

bibtex2html (1.76-1) unstable; urgency=low

  * New upstream version. This version fixes a bug with the translation of
    accented characters (closes: Bug#329398).
  * Added debian/svn-deblayout.
  * Standards-Version 3.6.2 (no change)

 -- Ralf Treinen <treinen@debian.org>  Thu, 22 Sep 2005 20:56:38 +0200

bibtex2html (1.75-2) unstable; urgency=medium

  * Bumped (build-)dependency on ocaml-* from 3.08 to 3.08.3

 -- Ralf Treinen <treinen@debian.org>  Wed, 23 Mar 2005 08:54:28 +0100

bibtex2html (1.75-1) unstable; urgency=low

  * New upstream version. This version reflects the change of the names of
    output files in the documentation.
  * Removed patch 05_outputfilenames since no longer necessary.
  * Adapted patch 06_doc-noexamples to the updated manual.

 -- Ralf Treinen <treinen@debian.org>  Sun,  6 Feb 2005 16:16:58 +0100

bibtex2html (1.74-2) unstable; urgency=low

  * Patch 05_outputfilenames: reflect the change of the names of output
    files in the documentation. Added note in NEWS.Debian about this.
    (closes: Bug#291828).
  * Patch 06_doc-noexamples: remove references to examples since these
    are not distributed by upstream.
  * New uploader Samuel Mimram.
  * Changed short description; starts now on lowercase.

 -- Ralf Treinen <treinen@debian.org>  Tue,  1 Feb 2005 22:07:29 +0100

bibtex2html (1.74-1) unstable; urgency=low

  * New upstream release.
    - fixes a bug concerning the table of contents of the manual
      (closes: Bug#274508).

 -- Ralf Treinen <treinen@debian.org>  Sun, 24 Oct 2004 15:48:31 +0200

bibtex2html (1.73-1) unstable; urgency=low

  * New upstream version
  * Adapted patch 03_charset to modfied main.ml file

 -- Ralf Treinen <treinen@debian.org>  Sun, 24 Oct 2004 15:39:02 +0200

bibtex2html (1.71-1) unstable; urgency=low

  * New upstream version.

 -- Ralf Treinen <treinen@debian.org>  Wed,  1 Sep 2004 08:23:04 +0200

bibtex2html (1.70-1) unstable; urgency=low

  * New upstream version
  * Ocaml version 3.08
  * Depend on ocaml-base-nox, build-depend on ocaml-nox

 -- Ralf Treinen <treinen@debian.org>  Thu, 22 Jul 2004 12:32:52 +0200

bibtex2html (1.69-1) unstable; urgency=low

  * New upstream version.

 -- Ralf Treinen <treinen@debian.org>  Tue, 20 Apr 2004 22:38:20 +0200

bibtex2html (1.68-1) unstable; urgency=low

  * New upstream version.
  * Added debian/watch

 -- Ralf Treinen <treinen@debian.org>  Wed, 17 Mar 2004 08:41:52 +0100

bibtex2html (1.66-1) unstable; urgency=low

  * New upstream version.

 -- Ralf Treinen <treinen@debian.org>  Thu, 19 Feb 2004 23:11:51 +0100

bibtex2html (1.65-2) unstable; urgency=low

  * patch 02_ampersand: quote correctly ampersands inside anchors. Thanks to
    Arnaud Giersch (closes: Bug#218310).
  * patch 03_charset: specify charset in a <meta> tag, take value from
    locale or command line option (closes: Bug#198802).

 -- Ralf Treinen <treinen@debian.org>  Mon,  8 Dec 2003 21:12:28 +0100

bibtex2html (1.65-1) unstable; urgency=low

  * New upstream version.

 -- Ralf Treinen <treinen@debian.org>  Fri,  3 Oct 2003 20:31:35 +0200

bibtex2html (1.64-1) unstable; urgency=low

  * New upstream release.
  * Standards-Version 3.6.1 (no changes).
  * Compile with ocaml-3.07.

 -- Ralf Treinen <treinen@debian.org>  Wed,  1 Oct 2003 22:23:42 +0200

bibtex2html (1.61-1) unstable; urgency=low

  * New upstream release
  * clean target in debian/rules calls "make dist-clean" now instead of
    "make clean"
  * File debian/compat instead of DH_COMPAT variable in debian/rules
  * Changed the Maintainer field to "Debian Ocaml Maintainers", added an
    Uploaders field with entries Sven Luther, Jerome Marant, Ralf Treinen,
    Remi Vanicat, Stefano Zacchiroli.
  * Standards-Version 3.6.0
  * Move to the dpatch system:
    - Add build-dependency on dpatch
    - Create directory debian/patches, put patch 01_manpage.dpatch there
    - debian/rules: include dpatch.make, have hlins.bc depend on patch,
      and clean depend on unpatch
  * debian/rules: replace dependency on configure-stamp target by
    dependency on Makefile.
  * Remove bogus files configure.old and auxtobib.

 -- Ralf Treinen <treinen@debian.org>  Tue, 15 Jul 2003 22:21:13 +0200

bibtex2html (1.56-1) unstable; urgency=low

  * New upstream release.
  * Recompiled with ocaml 3.06-13. Depends on
    ocaml-base-3.06-1. Build-depends on ocaml-3.06.1.
  * DH_COMPAT=4, build-depends on debhelper >= 4.0
  * debian/rules: added a configure target. clean target: rm config.log.

 -- Ralf Treinen <treinen@debian.org>  Mon, 16 Dec 2002 20:30:46 +0100

bibtex2html (1.54-3) unstable; urgency=low

  * Changed (build)-dependency to ocaml >= 3.06 & << 3.07, since future
    versions of ocaml are not guaranteed to work on 3.06 bytecode.
  * Removed from prerm and postinst obsolete code for handling the
    /usr/doc symlink.

 -- Ralf Treinen <treinen@debian.org>  Thu,  3 Oct 2002 22:35:28 +0200

bibtex2html (1.54-2) unstable; urgency=low

  * Recompiled for ocaml 3.06 (closes: Bug#156254), bumped up
    build-dependency on ocaml and dependency on ocaml-base to >= 3.06.
  * Makefile.in: changed the STRLIB variable to use dynamic str library from
    /usr/lib/ocaml/stublibs.

 -- Ralf Treinen <treinen@debian.org>  Fri, 30 Aug 2002 20:43:46 +0200

bibtex2html (1.54-1) unstable; urgency=low

  * New upstream release

 -- Ralf Treinen <treinen@debian.org>  Sun, 14 Jul 2002 22:19:00 +0200

bibtex2html (1.53-1) unstable; urgency=low

  * New upstream release (closes: Bug#151204).

 -- Ralf Treinen <treinen@debian.org>  Sat, 29 Jun 2002 09:30:31 +0200

bibtex2html (1.52-1) unstable; urgency=low

  * New upstream release
  * Remove debian/aux2bib.1 and dh_installmanpages in debian/rules since this
    man page is now in upstream source.

 -- Ralf Treinen <treinen@debian.org>  Tue, 15 Jan 2002 19:17:02 +0100

bibtex2html (1.51-2) unstable; urgency=low

  * Translate to bytecode, hence architecture=all and depend on ocaml-base.
    Add a target "install-byte" to Makefile.in. Removed dh_shlibdeps.
    Removed the -custom option from the "byte" target in Makefile.in.
  * Added depends on perl (needed for aux2bib).
  * Removed dynamic creation of the copyright file (which used to be needed in
    case of compilation to byte code with attached run time system).
  * Build-Depends-Indeps: specify version >= 3.0 for debhelper,
    remove autoconf.

 -- Ralf Treinen <treinen@debian.org>  Wed,  9 Jan 2002 19:44:05 +0100

bibtex2html (1.51-1) unstable; urgency=low

  * New upstream release (contains new auxtobib).
  * Some cleanup in rules file.
  * Use option to configure for setting of installation dirs, instead of
    patching Makefile.in.
  * Remove config.status and config.cache in clean target of rules.
  * Rename "auxtobib" executable to "aux2bib".
  * Modified aux2bib to use kpsewhich for searching bibtex file.
  * Added a manpage for aux2bib.
  * Fixed typos in description (closes: Bug#119780) and removed redundant
    formulations, add description of aux2bib.
  * Policy-Version 3.5.6.0.

 -- Ralf Treinen <treinen@debian.org>  Sun,  9 Dec 2001 14:28:34 +0100

bibtex2html (1.46-2) unstable; urgency=low

  * changed in debian/rules reference to debian/tmp to debian/bibtex2html
    (closes: Bug#107436)

 -- Ralf Treinen <treinen@debian.org>  Thu,  2 Aug 2001 12:12:09 +0200

bibtex2html (1.46-1) unstable; urgency=low

  * New upstream release
  * DH_COMPAT=3
  * Standards-Version: 3.5.5.0
  * do not install the upstream README which contains only installation
    instructions and stuff contained elsewhere. Remove debian/docs which
    no longer needed.
  * remove some debhelper cruft from debian/rules
  * added hevea to the build-depends (needed for the manual in html)

 -- Ralf Treinen <treinen@debian.org>  Fri,  6 Jul 2001 16:17:19 +0200

bibtex2html (1.44-1) unstable; urgency=low

  * New upstream release
  * changed in main Makefile.in MINORVN to 44
  * Added url of home page in the description field of control
  * rules: strip binary only when compiling to native

 -- Ralf Treinen <treinen@debian.org>  Thu, 11 Jan 2001 19:18:11 +0100

bibtex2html (1.43-3) unstable; urgency=low

  * Added in control file: Suggests: hlins
  * Man pages refer now to locally installed doc (closes: bug#74669)
  * fixed a typo in doc-base.bibtex2html

 -- Ralf Treinen <treinen@debian.org>  Sat,  9 Dec 2000 13:12:44 +0100

bibtex2html (1.43-2) unstable; urgency=low

  * Build-depends: added tetex-extra (closes: bug#72423).
  * Added some words in the abstract of the doc-base entry.

 -- Ralf Treinen <treinen@debian.org>  Tue, 26 Sep 2000 21:48:50 +0200

bibtex2html (1.43-1) unstable; urgency=low

  * New upstream release
  * Build-depends: add autoconf
  * Build-depends and Depends: remove tetex-lib
  * Architecture: any, add ocaml runtime licence for byte-code compiled
    binaries (closes: bug#71506).

 -- Ralf Treinen <treinen@debian.org>  Thu, 21 Sep 2000 22:30:16 +0200

bibtex2html (1.42-2) unstable; urgency=low

  * remove dependency of ocaml and restricted package to the architectures
    i386, alpha, powerpc.
    (this should be the list of architectures where ocaml can be compiled
    to native code).

 -- Ralf Treinen <treinen@debian.org>  Mon, 11 Sep 2000 22:24:29 +0200

Bibtex2html (1.42-1) unstable; urgency=low

  * New upstream release

 -- Ralf Treinen <treinen@debian.org>  Mon,  4 Sep 2000 22:28:29 +0200

bibtex2html (1.2-5) unstable; urgency=low

  * Add manual.html into the source package (and protect it from make
    clean). Temporary solution as long as hevea is not in debian.

  * register manual.html with doc-base

 -- Ralf Treinen <treinen@club-internet.fr>  Wed,  5 Jul 2000 20:40:22 +0200

bibtex2html (1.2-4) unstable; urgency=low

  * Syntax error in debian/prerm

 -- Ralf Treinen <treinen@club-internet.fr>  Sun,  7 May 2000 17:29:15 +0200

bibtex2html (1.2-3) unstable; urgency=low

  * Compile to bytecode when native code compiler not available
  * Added dependency on ocaml

 -- Ralf Treinen <treinen@club-internet.fr>  Sun,  7 May 2000 14:21:20 +0200

bibtex2html (1.2-2) unstable; urgency=low

  * Added man pages.

 -- Ralf Treinen <treinen@club-internet.fr>  Fri, 21 Apr 2000 20:30:05 +0200

bibtex2html (1.2-1) unstable; urgency=low

  * Initial Release.

 -- Ralf Treinen <treinen@club-internet.fr>  Fri, 10 Mar 2000 23:35:45 +0100
